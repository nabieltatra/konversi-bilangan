import java.util.Scanner;

public class KonversiBilangan {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        int pilihan;

        do {
            System.out.println("Menu Konversi Bilangan:");
            System.out.println("1. Biner ke Desimal");
            System.out.println("2. Desimal ke Biner");
            System.out.println("3. Biner ke Heksadesimal");
            System.out.println("4. Heksadesimal ke Biner");
            System.out.println("5. Desimal ke Heksadesimal");
            System.out.println("6. Heksadesimal ke Desimal");
            System.out.println("7. Keluar");
            System.out.print("Pilih opsi: ");
            pilihan = input.nextInt();

            switch (pilihan) {
                case 1:
                    System.out.print("Masukkan bilangan biner: ");
                    String biner = input.next();
                    int desimalDariBiner = Integer.parseInt(biner, 2);
                    System.out.println("Hasil konversi: " + desimalDariBiner);
                    break;
                case 2:
                    System.out.print("Masukkan bilangan desimal: ");
                    int desimal = input.nextInt();
                    String binerDariDesimal = Integer.toBinaryString(desimal);
                    System.out.println("Hasil konversi: " + binerDariDesimal);
                    break;
                case 3:
                    System.out.print("Masukkan bilangan biner: ");
                    String binerToHex = input.next();
                    int desimalFromBiner = Integer.parseInt(binerToHex, 2);
                    String heksadesimal = Integer.toHexString(desimalFromBiner);
                    System.out.println("Hasil konversi: " + heksadesimal.toUpperCase());
                    break;
                case 4:
                    System.out.print("Masukkan bilangan heksadesimal: ");
                    String heksadesimalToBin = input.next();
                    int desimalFromHex = Integer.parseInt(heksadesimalToBin, 16);
                    String binerFromHex = Integer.toBinaryString(desimalFromHex);
                    System.out.println("Hasil konversi: " + binerFromHex);
                    break;
                case 5:
                    System.out.print("Masukkan bilangan desimal: ");
                    int desimalToHex = input.nextInt();
                    String heksadesimalFromDesimal = Integer.toHexString(desimalToHex);
                    System.out.println("Hasil konversi: " + heksadesimalFromDesimal.toUpperCase());
                    break;
                case 6:
                    System.out.print("Masukkan bilangan heksadesimal: ");
                    String heksadesimalToDesimal = input.next();
                    int desimalFromHexToDesimal = Integer.parseInt(heksadesimalToDesimal, 16);
                    System.out.println("Hasil konversi: " + desimalFromHexToDesimal);
                    break;
                case 7:
                    System.out.println("Terima kasih! Keluar dari program.");
                    break;
                default:
                    System.out.println("Pilihan tidak valid. Silakan pilih kembali.");
            }
        } while (pilihan != 7);
    }
}